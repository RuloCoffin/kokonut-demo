package com.kokonut.rulo.kokonutdemo

import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.core.content.ContextCompat
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.kokonut.rulo.kokonutdemo.Base.Activity.BaseActivity
import com.kokonut.rulo.kokonutdemo.Core.PostsAllResponse
import com.kokonut.rulo.kokonutdemo.UI.UIUtils
import kotlinx.android.synthetic.main.activity_post_details.*

val EXTRA_POST = "extra_post"

class PostDetailsActivity : BaseActivity() {
    private lateinit var currentPost: PostsAllResponse.Post
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_details)
        setSupportActionBar(main_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!intent.hasExtra(EXTRA_POST)) {
            finish()
            return
        }
        currentPost = intent.getParcelableExtra(EXTRA_POST)
        fillData()
        fab_post.setOnClickListener {
            preferencesHelper?.toggleFavoritePost(currentPost.idPost.toString())
            validateFabState()
        }
    }



    private fun fillData() {
        supportActionBar?.title = currentPost.title
        validateFabState()
        main_backdrop.setImageDrawable(
            TextDrawable.builder()
                .buildRect(
                    currentPost.title.substring(0, 1).toUpperCase(),
                    ColorGenerator.MATERIAL.getColor(currentPost.title)
                )
        )

        UIUtils().setPostPicture(currentPost.imageUrl, main_backdrop, currentPost.title)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt_post.text = Html.fromHtml(currentPost.body, Html.FROM_HTML_MODE_COMPACT)
        } else {
            txt_post.text = Html.fromHtml(currentPost.body)
        }
    }

    private fun validateFabState() {
        if (!preferencesHelper!!.isFavoritePost(currentPost.idPost.toString())) {
            fab_post.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_white))
        } else {
            fab_post.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_favorite))
        }
    }
}
