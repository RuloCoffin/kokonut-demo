package com.kokonut.rulo.kokonutdemo

import android.content.Intent
import android.os.Bundle
import com.kokonut.rulo.kokonutdemo.Base.Activity.BaseActivity
import com.kokonut.rulo.kokonutdemo.DataUtils.PreferencesHelper

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val i = Intent()
        if (PreferencesHelper(this).getAccessToken() == null) {
            i.setClass(this, LoginActivity::class.java)
        } else {
            i.setClass(this, MainActivity::class.java)
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(i)
        finish()
    }
}
