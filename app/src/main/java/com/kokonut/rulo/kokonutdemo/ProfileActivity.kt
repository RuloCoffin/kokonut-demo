package com.kokonut.rulo.kokonutdemo

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.kokonut.rulo.kokonutdemo.Base.Activity.BaseActivity
import com.kokonut.rulo.kokonutdemo.UI.UIUtils
import kotlinx.android.synthetic.main.activity_profile.*
import java.util.*


class ProfileActivity : BaseActivity() {
    private val TAG = ProfileActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_profile)

        txt_profile_build.text = "Build ${BuildConfig.VERSION_NAME}"
        img_profile.setImageDrawable(
            TextDrawable.builder()
                .buildRound(
                    "?",
                    ColorGenerator.MATERIAL.getColor(UUID.randomUUID())
                )
        )

        btn_logout.setOnClickListener {
            showAlertOkCancel(
                getString(R.string.lbl_logout),
                "Are you sure?",
                DialogInterface.OnClickListener { _, _ ->
                    preferencesHelper?.clear()
                    val i = Intent(this@ProfileActivity, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(i)
                    finish()
                })
        }

        attemptFillData()

    }

    private fun attemptFillData() {
        if (preferencesHelper?.getLoggedUser() != null) {
            var user = preferencesHelper?.getLoggedUser()

            img_profile.setImageDrawable(
                TextDrawable.builder()
                    .buildRound(
                        user?.name!!.substring(0, 1),
                        ColorGenerator.MATERIAL.getColor(UUID.randomUUID())
                    )
            )

            UIUtils().setProfilePicture(user?.image, img_profile, user?.name!!)
            txt_profile_name.text = "${user?.name} ${user?.lastName}"
            txt_profile_email.text = user.email
        }
    }
}
