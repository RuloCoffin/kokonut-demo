package com.kokonut.rulo.kokonutdemo.Base.Fragment.DialogFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kokonut.rulo.kokonutdemo.R

class KokonutSpinnerDialog : BaseDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_kokonut_spinner, container, false)
    }

    companion object {

        fun newInstance(): KokonutSpinnerDialog {
            val fragment = KokonutSpinnerDialog()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}