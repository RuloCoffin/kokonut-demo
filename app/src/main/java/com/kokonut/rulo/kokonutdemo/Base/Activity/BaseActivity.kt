package com.kokonut.rulo.kokonutdemo.Base.Activity


import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kokonut.rulo.kokonutdemo.Base.Fragment.DialogFragment.KokonutSpinnerDialog
import com.kokonut.rulo.kokonutdemo.DataUtils.Log
import com.kokonut.rulo.kokonutdemo.DataUtils.PreferencesHelper
import com.kokonut.rulo.kokonutdemo.R


open class BaseActivity : AppCompatActivity() {
    private var kokonutSpinnerDialog: KokonutSpinnerDialog? = null
    private var TAG = BaseActivity::class.java.simpleName
    var preferencesHelper: PreferencesHelper? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferencesHelper = PreferencesHelper(this@BaseActivity)
    }

    fun showProgressAnim() {
        kokonutSpinnerDialog = KokonutSpinnerDialog.newInstance()
        kokonutSpinnerDialog?.show(supportFragmentManager, "SocorritoSpinnerDialog")
    }

    fun hideProgressAnim() {
        try {
            if (kokonutSpinnerDialog != null)
                kokonutSpinnerDialog?.dismiss()
        } catch (x: Exception) {
            Log.wtf(TAG, x.message)
            x.printStackTrace()
        }

    }

    fun showAlert(title: String, message: String, onClickListener: DialogInterface.OnClickListener?) {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(android.R.string.ok), onClickListener)
                .create().show()
        }
    }

    fun showAlert(@StringRes title: Int, @StringRes message: Int) {
        showAlert(getString(title), getString(message), null)
    }

    fun showAlertOkCancel(title: String, message: String, onClickListener: DialogInterface.OnClickListener?) {
        runOnUiThread {
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(getString(android.R.string.cancel), null)
                .setPositiveButton(getString(android.R.string.ok), onClickListener)
                .create().show()
        }
    }

    fun showError(@StringRes message: Int) {
        showAlert(getString(R.string.error_title), getString(message), null)
    }

    override fun onStop() {
        hideProgressAnim()
        super.onStop()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
