package com.kokonut.rulo.kokonutdemo.DataUtils

import com.kokonut.rulo.kokonutdemo.BuildConfig

object Log {
    private val MUST_LOG = BuildConfig.DEBUG

    fun i(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.i(tag, string)
    }

    fun e(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.e(tag, string)
    }

    fun d(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.wtf(tag, string)
    }

    fun v(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.v(tag, string)
    }

    fun w(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.wtf(tag, string)
    }

    fun wtf(tag: String, string: String?) {
        if (MUST_LOG) android.util.Log.wtf(tag, string)
    }
}