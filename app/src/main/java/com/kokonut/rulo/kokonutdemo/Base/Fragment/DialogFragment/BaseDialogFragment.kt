package com.kokonut.rulo.kokonutdemo.Base.Fragment.DialogFragment

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.LinearLayout


open class BaseDialogFragment : androidx.fragment.app.DialogFragment() {

    fun dismissDialog() {
        dialog.dismiss()
    }

    override fun onStart() {
        super.onStart()
        dialog.window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
    }
}