package com.kokonut.rulo.kokonutdemo

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.gson.Gson
import com.kokonut.rulo.kokonutdemo.Adapter.MainPostsAdapter
import com.kokonut.rulo.kokonutdemo.Base.Activity.BaseActivity
import com.kokonut.rulo.kokonutdemo.Core.PostsAllResponse
import com.kokonut.rulo.kokonutdemo.DataUtils.Log
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException


class MainActivity : BaseActivity(), IPostClickedListener {
    private val TAG = MainActivity::class.java.simpleName
    private var mainPostsAdapter: MainPostsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainPostsAdapter = MainPostsAdapter(this)

        //Avoid onItemChangedBlinks
        (recycler_main_posts.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        recycler_main_posts.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
        recycler_main_posts.adapter = mainPostsAdapter


        getData()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_profile) {
            startActivity(Intent(this, ProfileActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getData() {
        val request = Request.Builder()
            .url("http://dev.estandares.kokonutstudio.com:8080/api/post/all")
            .get()
            .addHeader("app_version", "Android ${BuildConfig.VERSION_NAME}")
            .addHeader("lang", "es_mx")
            .addHeader("Accept", "application/json")
            .build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val stringResponse = response.body()?.string()
                val postsAllResponse = Gson().fromJson(stringResponse, PostsAllResponse::class.java)
                Log.wtf(TAG, postsAllResponse.data?.posts?.toString())
                runOnUiThread {
                    mainPostsAdapter?.addItems(postsAllResponse.data?.posts)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
            }
        })
    }

    override fun onPostClicked(post: PostsAllResponse.Post) {
        Log.wtf(TAG, post.toString())
        val postIntent = Intent(this, PostDetailsActivity::class.java)
        postIntent.putExtra(EXTRA_POST, post)
        startActivity(postIntent)
    }

    override fun onRestart() {
        super.onRestart()
        mainPostsAdapter?.notifyDataSetChanged()
    }
}


interface IPostClickedListener {
    fun onPostClicked(post: PostsAllResponse.Post)
}
