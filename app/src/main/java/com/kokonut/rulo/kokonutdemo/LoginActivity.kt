package com.kokonut.rulo.kokonutdemo

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.google.gson.Gson
import com.kokonut.rulo.kokonutdemo.Base.Activity.BaseActivity
import com.kokonut.rulo.kokonutdemo.Core.AuthResponse
import com.kokonut.rulo.kokonutdemo.DataUtils.Log
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException


class LoginActivity : BaseActivity() {
    private var TAG = LoginActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // Set up the login form.
        email_sign_in_button.setOnClickListener { attemptLogin() }

        //ToDo Remove debug hardcode

        if (BuildConfig.DEBUG) {
            login_email.setText("android@kokonutstudio.com")
            login_password.setText("12345678")
        }
    }

    private fun attemptLogin() {
        til_login_email.error = null
        til_login_email.isErrorEnabled = false
        til_login_password.error = null
        til_login_password.isErrorEnabled = false


        val emailStr = login_email.text.toString()
        val passwordStr = login_password.text.toString()

        if (!TextUtils.isEmpty(emailStr)) {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
                til_login_email.isErrorEnabled = true
                til_login_email.error = "Invalid email"
                return
            }
        } else {
            til_login_email.isErrorEnabled = true
            til_login_email.error = "Required email"
            return
        }

        if (TextUtils.isEmpty(passwordStr)) {
            til_login_password.isErrorEnabled = true
            til_login_password.error = "Required password"
            return
        }
        login(emailStr, passwordStr)
    }


    fun login(email: String, pass: String) {
        showProgressAnim()
        val client = OkHttpClient()

        val mediaType = MediaType.parse("application/json")
        val obj: JSONObject = JSONObject()
        obj.put("username", email)
        obj.put("password", pass)
        val body = RequestBody.create(
            mediaType,
            obj.toString()
        )
        val request = Request.Builder()
            .url("http://dev.estandares.kokonutstudio.com:8080/api/login")
            .post(body)
            .addHeader("app_version", "Android ${BuildConfig.VERSION_NAME}")
            .addHeader("lang", "es_mx")
            .addHeader("Accept", "application/json")
            .addHeader("Content-Type", "application/json")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.v(TAG, e.message)
                hideProgressAnim()
                showError(R.string.error_default)
            }

            override fun onResponse(call: Call, response: Response) {

                var stringResponse = response.body()?.string()
                var jsonResponse = JSONObject(stringResponse)
                if (jsonResponse.optInt("success", 0) == 1) {
                    var responseObject = Gson().fromJson(stringResponse, AuthResponse::class.java)
                    Log.wtf(TAG, responseObject.toString())
                    preferencesHelper?.putAccessToken(responseObject.data?.accessToken)
                    var intent = Intent(this@LoginActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()
                } else {
                    hideProgressAnim()
                    showError(R.string.error_invalid_credentials)
                    Log.v(TAG, jsonResponse.toString())
                }


            }
        })
    }

}
