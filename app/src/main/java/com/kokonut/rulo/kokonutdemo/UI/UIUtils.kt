package com.kokonut.rulo.kokonutdemo.UI

import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.TextUtils
import android.widget.ImageView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.kokonut.rulo.kokonutdemo.R

class UIUtils {
    fun setProfilePicture(pictureUri: Uri?, target: ImageView) {
        if (pictureUri != null) {
            Glide.with(target.context).load(pictureUri)
                .apply(
                    RequestOptions()
                        .circleCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .error(R.drawable.ic_account)
                        .priority(Priority.IMMEDIATE)
                )
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(target)
        }
    }

    fun setProfilePicture(pictureUrl: String?, target: ImageView, postName: String) {
        if (!TextUtils.isEmpty(pictureUrl)) {
            Glide.with(target.context).load(pictureUrl)
                .apply(
                    RequestOptions()
                        .dontAnimate()
                        .circleCrop()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .priority(Priority.IMMEDIATE)
                )
                .into(object : Target<Drawable> {
                    override fun getRequest(): Request? {
                        return null
                    }

                    override fun onLoadStarted(placeholder: Drawable?) {

                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        target.setImageDrawable(
                            TextDrawable.builder()
                                .buildRound(
                                    postName.substring(0, 1).toUpperCase(),
                                    ColorGenerator.MATERIAL.getColor(postName)
                                )
                        )
                    }

                    override fun getSize(cb: SizeReadyCallback) {

                    }


                    override fun onStop() {

                    }

                    override fun setRequest(request: Request?) {

                    }

                    override fun removeCallback(cb: SizeReadyCallback) {

                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                        target.setImageDrawable(resource)
                    }

                    override fun onStart() {

                    }

                    override fun onDestroy() {

                    }
                })
        } else {
            try {
                target.setImageDrawable(
                    TextDrawable.builder()
                        .buildRound(postName.substring(0, 1).toUpperCase(), ColorGenerator.MATERIAL.getColor(postName))
                )
            } catch (x: Exception) {
                x.printStackTrace()

            }
        }
    }

    fun setPostPicture(pictureUrl: String?, target: ImageView, postName: String) {
        if (!TextUtils.isEmpty(pictureUrl)) {
            Glide.with(target.context).load(pictureUrl)
                .apply(
                    RequestOptions()
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .priority(Priority.IMMEDIATE)
                )
                .into(object : Target<Drawable> {
                    override fun getRequest(): Request? {
                        return null
                    }

                    override fun onLoadStarted(placeholder: Drawable?) {

                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        target.setImageDrawable(
                            TextDrawable.builder()
                                .buildRect(
                                    postName.substring(0, 1).toUpperCase(),
                                    ColorGenerator.MATERIAL.getColor(postName)
                                )
                        )
                    }

                    override fun getSize(cb: SizeReadyCallback) {

                    }


                    override fun onStop() {

                    }

                    override fun setRequest(request: Request?) {

                    }

                    override fun removeCallback(cb: SizeReadyCallback) {

                    }

                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                        target.setImageDrawable(resource)
                    }

                    override fun onStart() {

                    }

                    override fun onDestroy() {

                    }
                })
        } else {
            try {
                target.setImageDrawable(
                    TextDrawable.builder()
                        .buildRect(postName.substring(0, 1).toUpperCase(), ColorGenerator.MATERIAL.getColor(postName))
                )
            } catch (x: Exception) {
                x.printStackTrace()

            }
        }
    }
}
