package com.kokonut.rulo.kokonutdemo.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.kokonut.rulo.kokonutdemo.Core.PostsAllResponse
import com.kokonut.rulo.kokonutdemo.DataUtils.Log
import com.kokonut.rulo.kokonutdemo.DataUtils.PreferencesHelper
import com.kokonut.rulo.kokonutdemo.IPostClickedListener
import com.kokonut.rulo.kokonutdemo.R
import com.kokonut.rulo.kokonutdemo.UI.UIUtils
import kotlinx.android.synthetic.main.post_item_layout.view.*

class MainPostsAdapter(context: Context) : RecyclerView.Adapter<MainPostsAdapter.PostHolder>() {
    val TAG = MainPostsAdapter::class.java.simpleName
    var mContext = context;
    var inflater: LayoutInflater? = LayoutInflater.from(context)
    var preferencesHelper = PreferencesHelper(context)
    var uiUtils = UIUtils()
    var iPostClickedListener: IPostClickedListener? = null

    init {
        if (context is IPostClickedListener)
            iPostClickedListener = context
    }

    private val postsList = SortedList<PostsAllResponse.Post>(
        PostsAllResponse.Post::class.java,
        object : SortedList.Callback<PostsAllResponse.Post>() {
            override fun areItemsTheSame(item1: PostsAllResponse.Post?, item2: PostsAllResponse.Post?): Boolean {
                return item1?.idPost == item2?.idPost
            }

            override fun onMoved(fromPosition: Int, toPosition: Int) {
                notifyItemMoved(fromPosition, toPosition)
            }

            override fun onChanged(position: Int, count: Int) {
                notifyItemRangeChanged(position, count)
            }

            override fun onInserted(position: Int, count: Int) {
                notifyItemRangeInserted(position, count)
            }

            override fun onRemoved(position: Int, count: Int) {
                notifyItemRangeRemoved(position, count)
            }

            override fun compare(o1: PostsAllResponse.Post?, o2: PostsAllResponse.Post?): Int {
                return o1?.title!!.compareTo(o2?.title!!)
            }

            override fun areContentsTheSame(oldItem: PostsAllResponse.Post?, newItem: PostsAllResponse.Post?): Boolean {
                return oldItem.toString().equals(newItem.toString())
            }
        })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        return PostHolder(inflater!!.inflate(R.layout.post_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return postsList.size()
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.bind(postsList[position])
    }

    fun addItems(items: List<PostsAllResponse.Post>?) {
        if (items != null)
            postsList.addAll(items)
    }

    open inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(post: PostsAllResponse.Post) {
            itemView.txt_post_item.text = post.title

            itemView.img_post_item.setImageDrawable(
                TextDrawable.builder()
                    .buildRect(
                        post.title.substring(0, 1).toUpperCase(),
                        ColorGenerator.MATERIAL.getColor(post.title)
                    )
            )


             UIUtils().setPostPicture(post.imageUrl, itemView.img_post_item, post.title)

            itemView.btn_post_item.setImageDrawable(
                ContextCompat.getDrawable(
                    mContext, if (preferencesHelper.isFavoritePost(post.idPost.toString())) {
                        R.drawable.ic_favorite
                    } else {
                        R.drawable.ic_favorite_border
                    }
                )
            )
            itemView.btn_post_item.setOnClickListener {
                preferencesHelper.toggleFavoritePost(post.idPost.toString())
                itemView.btn_post_item.setImageDrawable(
                    ContextCompat.getDrawable(
                        mContext, if (preferencesHelper.isFavoritePost(post.idPost.toString())) {
                            R.drawable.ic_favorite
                        } else {
                            R.drawable.ic_favorite_border
                        }
                    )
                )


                //  notifyItemChanged(adapterPosition)
            }
            itemView.itm_post_item.setOnClickListener {
                Log.wtf(TAG, "Clicked " + post.toString())
                iPostClickedListener?.onPostClicked(post)

            }
        }
    }
}