package com.kokonut.rulo.kokonutdemo.Core

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class AuthResponse(
    @SerializedName("success")
    @Expose
    var success: Int? = null,
    @SerializedName("message")
    @Expose
    var message: String? = null,
    @SerializedName("data")
    @Expose
    var data: Data? = AuthResponse.Data()
) {


     data class Data( @SerializedName("token_type")
                       @Expose
                       var tokenType: String? = null,
                       @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null,
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null,
    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null)
}