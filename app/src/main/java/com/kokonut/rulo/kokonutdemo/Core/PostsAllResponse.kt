package com.kokonut.rulo.kokonutdemo.Core

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostsAllResponse(
    @SerializedName("success")
    @Expose
    var success: Int? = null,
    @SerializedName("message")
    @Expose
    var message: String? = null,
    @SerializedName("data")
    @Expose
    var data: Data? = null
) : Parcelable {
    @Parcelize
    data class Data(
        @SerializedName("current_page")
        @Expose
        var currentPage: Int? = null,
        @SerializedName("data")
        @Expose
        var posts: List<Post>? = null,
        @SerializedName("first_page_url")
        @Expose
        var firstPageUrl: String? = null,
        @SerializedName("from")
        @Expose
        var from: Int? = null,
        @SerializedName("last_page")
        @Expose
        var lastPage: Int? = null,
        @SerializedName("last_page_url")
        @Expose
        var lastPageUrl: String? = null,
        @SerializedName("next_page_url")
        @Expose
        var nextPageUrl: String? = null,
        @SerializedName("path")
        @Expose
        var path: String? = null,
        @SerializedName("per_page")
        @Expose
        var perPage: Int? = null,
        @SerializedName("prev_page_url")
        @Expose
        var prevPageUrl: String? = null,
        @SerializedName("to")
        @Expose
        var to: Int? = null,
        @SerializedName("total")
        @Expose
        var total: Int? = null
    ) : Parcelable

    @Parcelize
    data class Post(
        @SerializedName("id_post")
        @Expose
        var idPost: Int? = null,
        @SerializedName("title")
        @Expose
        var title: String = "",
        @SerializedName("body")
        @Expose
        var body: String? = null,
        @SerializedName("slug")
        @Expose
        var slug: String? = null,
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null,
        @SerializedName("header")
        @Expose
        var header: String? = null,
        @SerializedName("footer")
        @Expose
        var footer: String? = null,
        @SerializedName("image_url")
        @Expose
        var imageUrl: String? = null,
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
    ) : Parcelable
}



