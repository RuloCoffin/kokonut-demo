package com.kokonut.rulo.kokonutdemo.DataUtils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kokonut.rulo.kokonutdemo.Core.UserResponse
import okhttp3.*
import org.json.JSONObject
import java.io.IOException


class PreferencesHelper(context: Context?) {
    private val TAG = PreferencesHelper::class.java.simpleName
    private val preferences: SharedPreferences = context!!.getSharedPreferences("KOKONUT_PREFS", Context.MODE_PRIVATE)
    private val FAVORITES_SET = "FAVORITES_SET"
    private val ACCESS_TOKEN = "ACCESS_TOKEN"
    private val LOGGED_USER = "LOGGED_USER"

    fun getFavoritePosts(): HashSet<String> {
        val formatted = preferences.getString(FAVORITES_SET, null)
        return if (formatted != null) {
            Gson().fromJson(formatted, object : TypeToken<HashSet<String>>() {}.type)
        } else {
            HashSet()
        }
    }

    private fun putFavoritePost(postKey: String) {
        val edit = preferences.edit()
        val set = getFavoritePosts()
        set.add(postKey)
        edit.putString(FAVORITES_SET, Gson().toJson(set))
        edit.apply()

    }

    private fun removeFavoritePost(postKey: String) {
        val edit = preferences.edit()
        val set = getFavoritePosts()
        set.remove(postKey)
        edit.putString(FAVORITES_SET, Gson().toJson(set))
        edit.apply()
    }

    fun toggleFavoritePost(postKey: String) {
        if (isFavoritePost(postKey)) {
            removeFavoritePost(postKey)
        } else {
            putFavoritePost(postKey)
        }
        Log.wtf(TAG, getFavoritePosts().toString())
    }

    fun isFavoritePost(postKey: String): Boolean {
        return getFavoritePosts().contains(postKey)
    }

    fun putAccessToken(accessToken: String?) {
        if (accessToken != null) {
            val edit = preferences.edit()
            edit.putString(ACCESS_TOKEN, accessToken)
            edit.apply()
            getUserProfile()
        }
    }

    fun getAccessToken(): String? {
        return preferences.getString(ACCESS_TOKEN, null)
    }

    private fun getUserProfile() {
        val request = Request.Builder()
            .url("http://dev.estandares.kokonutstudio.com:8080/api/user/profile")
            .get()
            .addHeader("app_version", "Android 1.0")
            .addHeader("lang", "es_mx")
            .addHeader("Accept", "application/json")
            .addHeader("Authorization", "Bearer ${getAccessToken()}")
            .build()

        OkHttpClient().newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.wtf(TAG, e.message)
            }

            override fun onResponse(call: Call, response: Response) {
                var o = JSONObject(response.body()?.string())
                Log.wtf(TAG, o.toString())
                if (o.optInt("success", -1) == 1) {
                    var r = Gson().fromJson(o.toString(), UserResponse::class.java)
                    putLoggedUser(r.data)
                }
            }
        })
    }

    fun getLoggedUser(): UserResponse.Data? {
        return try {
            Gson().fromJson(preferences.getString(LOGGED_USER, null), UserResponse.Data::class.java)
        } catch (x: Exception) {
            null
        }
    }

    fun putLoggedUser(user: UserResponse.Data?) {
        if (user != null) {
            val edit = preferences.edit()
            edit.putString(LOGGED_USER, Gson().toJson(user))
            edit.apply()
            Log.wtf(TAG, getLoggedUser().toString())
        }
    }

    fun clear() {
        val edit = preferences.edit()
        edit.clear()
        edit.commit()
        edit.apply()
    }
}


